package builder

import (
	"fmt"
	"reflect"
	"testing"
)

func TestMakeBeverage(t *testing.T) {
	bd := &BeverageDirector{}
	b := &Beverage{}

	tb := &TeaBuilder{}
	tea, err := bd.MakeBeverage(tb, "Green Tea", " Honey", "No Milk")
	if err != nil {
		panic(err)
	}

	if len(tb.Milk) == 0 {
		t.Error("Excepting Milk to be set for TeaBuilder. But it did not set")
	}
	if len(tb.Sweetner) == 0 {
		t.Error("Excepting Sweetner to be set for TeaBuilder. But it did not set")
	}
	if len(tb.Powder) == 0 {
		t.Error("Excepting Powder to be set for TeaBuilder. But it did not set")
	}

	if reflect.TypeOf(tea) != reflect.TypeOf(b) {
		t.Errorf("Excepted Beverage struct type, but returned : %v", tea)
	}

	fmt.Printf("Tea: %+v\n", tea)

	cb := &CoffeeBuilder{}
	coffee, err := bd.MakeBeverage(cb, "Decafe", "White sugar", "Half and half")
	if err != nil {
		panic(err)
	}

	if len(cb.Milk) == 0 {
		t.Error("Excepting Milk to be set for CoffeeBuilder. But it did not set")
	}
	if len(cb.Sweetner) == 0 {
		t.Error("Excepting Sweetner to be set for CoffeeBuilder. But it did not set")
	}
	if len(cb.Powder) == 0 {
		t.Error("Excepting Powder to be set for CoffeeBuilder. But it did not set")
	}

	if reflect.TypeOf(coffee) != reflect.TypeOf(b) {
		t.Errorf("Excepted Beverage struct type, but returned : %v", coffee)
	}
	fmt.Printf("Coffee: %+v\n", coffee)

}
