package builder

import (
	"encoding/json"
)

// Beverage is the Product object in Builder Design Pattern
type Beverage struct {
	Tea    string
	Coffee string
}

// BeverageInterface is the inteface that every concrete implementation should obey
type BeverageInterface interface {
	SetPowder(powder string)
	SetSweetner(sweetner string)
	SetMilk(milk string)
	Beverage() (*Beverage, error)
}

// TeaBuilder is concrete builder
type TeaBuilder struct {
	Powder   string
	Sweetner string
	Milk     string
}

// CoffeeBuilder is concrete builder
type CoffeeBuilder struct {
	Powder   string
	Sweetner string
	Milk     string
}

// BeverageDirector is the Director in Builder Design Pattern
type BeverageDirector struct{}

//SetPowder interface method for TeaBuilder
func (t *TeaBuilder) SetPowder(p string) {
	t.Powder = p
}

//SetPowder interace method for CoffeeBuilder
func (c *CoffeeBuilder) SetPowder(p string) {
	c.Powder = p
}

//SetSweetner interface method for TeaBuilder
func (t *TeaBuilder) SetSweetner(s string) {
	t.Sweetner = s
}

//SetSweetner interace method for CoffeeBuilder
func (c *CoffeeBuilder) SetSweetner(s string) {
	c.Sweetner = s
}

//SetMilk interface method for TeaBuilder
func (t *TeaBuilder) SetMilk(m string) {
	t.Milk = m
}

//SetMilk interace method for CoffeeBuilder
func (c *CoffeeBuilder) SetMilk(m string) {
	c.Milk = m
}

//Beverage interface method for TeaBuilder
func (t *TeaBuilder) Beverage() (*Beverage, error) {
	tea := make(map[string]string)
	tea["Powder"] = t.Powder
	tea["Sweetner"] = t.Sweetner
	tea["Milk"] = t.Milk

	data, err := json.Marshal(tea)
	if err != nil {
		return nil, err
	}

	return &Beverage{Tea: string(data)}, nil
}

//Beverage interface method for CoffeeBuilder
func (c *CoffeeBuilder) Beverage() (*Beverage, error) {
	coffee := make(map[string]string)
	coffee["Powder"] = c.Powder
	coffee["Sweetner"] = c.Sweetner
	coffee["Milk"] = c.Milk

	data, err := json.Marshal(coffee)
	if err != nil {
		return nil, err
	}

	return &Beverage{Coffee: string(data)}, nil
}

//MakeBeverage a concrete Beverage via BeverageInterface
func (bd *BeverageDirector) MakeBeverage(builder BeverageInterface, powder string, sweetner string, milk string) (*Beverage, error) {
	builder.SetPowder(powder)
	builder.SetSweetner(sweetner)
	builder.SetMilk(milk)
	return builder.Beverage()
}
