package singletonSimple

import "testing"

func TestGetInstance(t *testing.T) {
	//TEST 1
	counter1 := GetInstance()
	if counter1 == nil {
		t.Error("Excepted pointer to singleton after GetInstance(), not nil")
	}

	exceptedInstance := counter1

	currentCounter := counter1.IncrementCounter()
	if currentCounter != 1 {
		t.Errorf("After calling first time IncrementCounter(), counter must be 1. But it is %d\n", currentCounter)
	}

	//TEST 2
	counter2 := GetInstance()
	if counter2 != exceptedInstance {
		t.Error("Excepted same instance in counter2 but it got different instance")
	}

	currentCounter = counter2.IncrementCounter()
	if currentCounter != 2 {
		t.Errorf("After caliing IncrementCounter() second time, counter must be 2 but it is: %d\n", currentCounter)
	}
}
