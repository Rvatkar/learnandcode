package singletonSimple

//Singleton struct
type Singleton struct {
	counter int
}

var instance *Singleton

//GetInstance creates Singleton instance
func GetInstance() *Singleton {
	if instance == nil {
		instance = new(Singleton)
	}
	return instance
}

//IncrementCounter increment counter by one
func (s *Singleton) IncrementCounter() int {
	s.counter++
	return s.counter
}
