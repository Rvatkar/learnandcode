package beverage

type Beverage struct {
	Name  string
	Price float64
}
