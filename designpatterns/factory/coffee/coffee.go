package coffee

import (
	"fmt"

	b "bitbucket.org/Rvatkar/learnandcode/designpatterns/factory/beverage"
)

type Coffee struct {
	cBeverages []*b.Beverage
}

func (c *Coffee) SetMenu() {
	coffeeList := make([]*b.Beverage, 0)

	bev := new(b.Beverage)
	bev.Name = "Coffee1"
	bev.Price = 3.99
	coffeeList = append(coffeeList, bev)

	bev = new(b.Beverage)
	bev.Name = "Coffee2"
	bev.Price = 4.99
	coffeeList = append(coffeeList, bev)

	c.cBeverages = coffeeList
	//fmt.Println("Coffee Set Menu")
}

func (c *Coffee) GetMenu() {
	//fmt.Println("Coffee Get Menu")

	for _, cc := range c.cBeverages {
		fmt.Printf("%+v\n", *cc)
	}

}
