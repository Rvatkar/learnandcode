package ifactory

import (
	"errors"

	c "bitbucket.org/Rvatkar/learnandcode/designpatterns/factory/coffee"
	t "bitbucket.org/Rvatkar/learnandcode/designpatterns/factory/tea"
)

type Beverages interface {
	SetMenu()
	GetMenu()
}

const (
	TEA = iota
	COFFEE
)

func BeveragesFactory(beverageType int) (Beverages, error) {
	switch beverageType {
	case TEA:
		return new(t.Tea), nil
	case COFFEE:
		return new(c.Coffee), nil
	default:
		return nil, errors.New("Unknown beverage")
	}
}
