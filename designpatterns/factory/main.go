package main

import (
	"fmt"

	"bitbucket.org/Rvatkar/learnandcode/designpatterns/factory/ifactory"
)

/*
Factory design pattern is a creational design pattern and it is also one of the most commonly used
pattern. This pattern provides a way to hide the creation logic of the instances being created.
The client only interacts with a factory struct and tells the kind of instances that needs to be
created. The factory class interacts with the corresponding concrete structs and returns the correct
instance back.
*/

func executeBeverage(bType int) {
	bInstance, err := ifactory.BeveragesFactory(bType)
	if err == nil {
		bInstance.SetMenu()
		bInstance.GetMenu()
	} else {
		fmt.Println(err)
	}
}

func main() {

	executeBeverage(ifactory.TEA)
	executeBeverage(ifactory.COFFEE)
}
