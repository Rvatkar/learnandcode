package tea

import (
	"fmt"

	b "bitbucket.org/Rvatkar/learnandcode/designpatterns/factory/beverage"
)

type Tea struct {
	tBeverages []*b.Beverage
}

func (t *Tea) SetMenu() {

	teaList := make([]*b.Beverage, 0)

	bev := new(b.Beverage)
	bev.Name = "Tea1"
	bev.Price = 1.99
	teaList = append(teaList, bev)

	bev = new(b.Beverage)
	bev.Name = "Tea2"
	bev.Price = 2.99
	teaList = append(teaList, bev)

	t.tBeverages = teaList
	//fmt.Println("Tea Set Menu")

}

func (t *Tea) GetMenu() {
	//fmt.Println("Tea Get Menu")

	for _, tt := range t.tBeverages {
		fmt.Printf("%+v\n", *tt)
	}
}
