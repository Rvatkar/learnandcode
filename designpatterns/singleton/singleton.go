package main

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var once sync.Once

type singleton struct {
	db *sql.DB
}

var dbInstance *singleton

func getDBInstance() *singleton {

	if dbInstance == nil {
		once.Do(func() {

			fmt.Println("Creating singleton DB instance...")

			dbConn, err := sql.Open("mysql", "test:test@tcp(localhost:3306)/hello")
			if err != nil {
				panic(err.Error())
			}
			//defer dbConn.Close()

			if err = dbConn.Ping(); err != nil {
				panic(err.Error())
			}

			dbInstance = &singleton{dbConn}
		})
	} else {
		fmt.Println("Single Instance already created")
	}

	return dbInstance
}

func main() {

	go func() {
		time.Sleep(time.Millisecond * 100)
		fmt.Println(*getDBInstance())
	}()

	for i := 0; i < 20; i++ {
		go func(counter int) {
			time.Sleep(time.Millisecond * 60)
			fmt.Printf("counter: %d %+v\n", counter, getDBInstance().db)
		}(i)
	}
	defer getDBInstance().db.Close()
	fmt.Scanln()
}
