package reversestring

import "strings"

/*
ReverseByArrayLen using for loop
*/
func ReverseByArrayLen(word string) string {
	var rev string
	for i := len(word) - 1; i >= 0; i-- {
		rev = rev + string(word[i])
	}
	return rev
}

/*
ReverseByArray using for loop
*/
func ReverseByArray(word string) string {
	var rev string
	for i := 0; i <= len(word)-1; i++ {
		rev = string(word[i]) + rev
	}
	return rev
}

/*
ReverseByBuilder using efficient string builder concentation
*/
func ReverseByBuilder(word string) string {
	var rev strings.Builder
	for i := len(word) - 1; i >= 0; i-- {
		rev.WriteByte(word[i])
	}
	return rev.String()

}

/*
ReverseByRune reverse string using rune

Besides the axiomatic detail that Go source code is UTF-8, there's really only one way that
Go treats UTF-8 specially, and that is when using a for range loop on a string.

We've seen what happens with a regular for loop. A for range loop, by contrast,
decodes one UTF-8-encoded rune on each iteration. Each time around the loop, the index of the loop is
the starting position of the current rune, measured in bytes, and the code point is its value.
For more info: https://blog.golang.org/strings
*/
func ReverseByRune(word string) string {
	var rev string
	for _, r := range word {
		rev = string(r) + rev
	}
	return rev
}
