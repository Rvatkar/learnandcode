package reversestring

import (
	"fmt"
	"testing"
)

func TestReverseByArrayLen(t *testing.T) {
	var word, want = "reshma", "amhser"
	t.Run(fmt.Sprintf("%v", word), func(t *testing.T) {
		got := ReverseByArrayLen(word)
		if got != want {
			t.Fatalf("Reverse want: %v Got: %v", want, got)
		}
	})
}

func TestReverseByArray(t *testing.T) {
	var word, want = "reshma", "amhser"
	t.Run(fmt.Sprintf("%v", word), func(t *testing.T) {
		got := ReverseByArray(word)
		if got != want {
			t.Fatalf("Reverse want: %v Got: %v", want, got)
		}
	})
}

func TestReverseByBuilder(t *testing.T) {
	var word, want = "reshma", "amhser"
	t.Run(fmt.Sprintf("%v", word), func(t *testing.T) {
		got := ReverseByBuilder(word)
		if got != want {
			t.Fatalf("Reverse want: %v Got: %v", want, got)
		}
	})
}

func TestReverseByRune(t *testing.T) {
	var word, want = "reshma", "amhser"
	t.Run(fmt.Sprintf("%v", word), func(t *testing.T) {
		got := ReverseByRune(word)
		if got != want {
			t.Fatalf("Reverse want: %v Got: %v", want, got)
		}
	})
}
