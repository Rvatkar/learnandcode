package main

import "fmt"

func main() {
	fmt.Println("main()")
	/*foo()
	fmt.Println()
	fooWithPanic()
	fmt.Println()*/
	f()
	fmt.Println("Returned normally from f.")
}

func foo() {
	fmt.Println("Inside foo()")
	defer bar()
}

func bar() {
	fmt.Println("Inside bar()")
	//During normal operation the recover function returns nil
	err := recover()
	fmt.Printf("err from recover() without panic = %+v", err)
}

func fooWithPanic() {
	fmt.Println("Inside fooWithPanic()")
	defer barWithPanic()
	panic("Any argument")
}

func barWithPanic() {
	fmt.Println("Inside barWithPanic()")
	//When panic() is called, the recover function returns whatever we gave as argument to the panic function:
	err := recover()
	fmt.Printf("err from recover() with panic = %+v", err)
}

func f() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f, Panic value from g: ", r)
		}
	}()
	fmt.Println("Calling g.")
	g(0)
	fmt.Println("Returned normally from g.")
}

func g(i int) {
	if i > 3 {
		fmt.Println("Panicking...")
		panic(fmt.Sprintf("%v", i))
	}
	defer fmt.Printf("Defer in g = %d\n", i)
	fmt.Println("Printing in g :", i)
	g(i + 1)
}
