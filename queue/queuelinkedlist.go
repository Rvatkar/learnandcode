package main

import "fmt"

type node struct {
	data int
	next *node
}

//queue - FIFO
type queue struct {
	front *node
	rear  *node
}

//enqueue : add an item to the queue
func (q *queue) enqueue(value int) {

	newNode := &node{data: value}

	// If queue is empty, then new node is front and rear both
	if q.rear == nil {
		q.front = newNode
		q.rear = newNode
		return
	}

	// Add the new node at the end of queue and change rear
	q.rear.next = newNode
	q.rear = newNode
}

// dequeue: remove an item from the queue
func (q *queue) dequeue() {

	if q.front == nil {
		fmt.Println("Queue is empty")
		return
	}

	fmt.Printf("%d popped from queue\n", q.front.data)
	q.front = q.front.next

	if q.front == nil {
		q.rear = nil
	}
}

func (q *queue) traverse() {

	if q.front == nil {
		fmt.Println("Queue is empty")
		return
	}

	frontNode := q.front

	for frontNode != nil {
		//fmt.Printf(" <- [ %+v ]", frontNode)
		fmt.Printf(" <- [ %d ]", frontNode.data)

		frontNode = frontNode.next
	}

	fmt.Println("")

}

//peek: Get the front and last item from queue.
func (q *queue) peek() {

	if q.rear == nil && q.front == nil {
		fmt.Println("Queue is empty")
		return
	}

	fmt.Printf("[ %d ] Front value\n", q.front.data)
	fmt.Printf("[ %d ] Rear value\n", q.rear.data)

}

func main() {
	q := &queue{}

	for i := 1; i < 6; i++ {
		q.enqueue(i)
	}

	q.traverse()
	q.peek()

	for i := 1; i < 6; i++ {
		q.dequeue()
		q.peek()
		q.traverse()
	}

}
