## Data structures & algorithms in Golang
Each package are individual package.

1.  Singly Linked List - User interactive
2.  Doubly Linked List
3.  Circular Linked List
     * Also have method to find if given list is circular or linear
4.  Stack - using linked list
5.  Queue - using linked list
6.  Sorting
     - Quick Sort
     - Insertion Sort
     - Merge Sort
7.  Binary search tree (BST)    
8.  Panic, recover
9.  Design patterns
     - Singleton pattern
     - Factory pattern
     - Builder pattern
10. Concurrency    
11. Reverse string with multiple ways