package main

import (
	"fmt"
)

type node struct {
	data int
	next *node
}

type list struct {
	head *node
}

func (l *list) createCircularList(value int) {

	newNode := &node{data: value}

	//If list is empty, insert node and points to itself
	if l.head == nil {
		l.head = newNode
		l.head.next = l.head
		return
	}

	currentNode := l.head
	for currentNode != nil {
		if currentNode.next == l.head {
			break
		}
		currentNode = currentNode.next

	}

	currentNode.next = newNode
	newNode.next = l.head
}

func (l *list) createLinearList(number int) {

	newNode := &node{data: number}

	if l.head == nil {
		l.head = newNode
		return
	}

	/* Store head node in current node and iterate till tail of list */
	currentNode := l.head
	for currentNode.next != nil {
		currentNode = currentNode.next
	}
	/* append new node to tail of list */
	currentNode.next = newNode

}

func (l *list) traverseCircularList() {
	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	//traversal list
	list := l.head
	for list != nil {
		fmt.Printf("[ %d (%v) ]->", list.data, list.next)
		//fmt.Printf("%d -> ", list.data)
		if list.next == l.head {
			break
		}
		list = list.next
	}
	fmt.Println("")
}

func (l *list) traverseLinearList() {

	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	//traversal list
	list := l.head
	for list != nil {
		fmt.Printf("[ %d (%v) ]->", list.data, list.next)
		//fmt.Printf("%d -> ", list.data)
		list = list.next
	}
	fmt.Println("")
}

func (l *list) isCircularList() {

	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	isCircular := false
	currentNode := l.head
	for currentNode != nil {
		if currentNode.next == l.head {
			isCircular = true
			break
		}
		currentNode = currentNode.next
	}

	fmt.Printf("LIST IF CIRCULAR: %t\n", isCircular)
}

func main() {
	fmt.Println("CIRCULAR LINKED LIST")

	head := &list{}
	for i := 1; i <= 5; i++ {
		head.createCircularList(i)
	}

	head.traverseCircularList()
	head.isCircularList()

	fmt.Println("LINEAR LINKED LIST")

	list := &list{}
	for i := 1; i <= 5; i++ {
		list.createLinearList(i)
	}

	list.traverseLinearList()
	list.isCircularList()
}
