package binarysearchtree

import (
	"errors"
)

/*
Node struct to create new Node
*/
type Node struct {
	Data  int
	Left  *Node
	Right *Node
}

/*
Tree ...
*/
type Tree struct {
	Root *Node
}

/*
InsertTailRecursively check data, create node and insert data recursively
*/
func (t *Tree) InsertTailRecursively(data int) {
	t.insert(t.Root, data)
}

func (t *Tree) insert(node *Node, data int) *Node {
	if t.Root == nil {
		t.Root = &Node{Data: data}
		return t.Root
	}

	switch {
	case node == nil:
		return &Node{Data: data}
	case data < node.Data:
		node.Left = t.insert(node.Left, data)
	case data > node.Data:
		node.Right = t.insert(node.Right, data)
	}
	return node
}

/*
InsertIteratively check data, create node and insert data iteratively
*/
func (t *Tree) InsertIteratively(data int) {
	if t.Root == nil {
		t.Root = createNode(data)
		return
	}

	//lets find position
	var currentNode *Node = nil
	root := t.Root
	for root != nil {
		currentNode = root
		switch {
		case data == root.Data:
			return
		case data < root.Data:
			root = root.Left
		case data > root.Data:
			root = root.Right
		}
	}

	//insert data
	if data < currentNode.Data {
		currentNode.Left = createNode(data)
	} else {
		currentNode.Right = createNode(data)
	}
}

func createNode(data int) *Node {
	return &Node{Data: data}
}

/*
InOrderTraverse using first class function
*/
func (t *Tree) InOrderTraverse(f func(data int)) {
	inOrderTraverse(t.Root, f)
}

func inOrderTraverse(n *Node, f func(data int)) {
	if n != nil {
		inOrderTraverse(n.Left, f)
		f(n.Data)
		inOrderTraverse(n.Right, f)
	}
}

/*
SearchNode finds node for given data value
*/
func (t *Tree) SearchNode(data int) bool {

	return t.Root.search(data)
}

/*
search given node for specific data
*/
func (n *Node) search(data int) bool {
	if n == nil {
		return false
	}

	if data < n.Data {
		return n.Left.search(data)
	}
	if data > n.Data {
		return n.Right.search(data)
	}
	return true
}

/*
DeleteNode deletes a provided data node
*/
func (t *Tree) DeleteNode(data int) error {
	parent := &Node{Right: t.Root}
	err := t.Root.deleteNode(data, parent)
	if err != nil {
		return err
	}

	if parent.Right == nil {
		t.Root = nil
	}

	return nil
}

func (n *Node) replaceNode(parent, replacewith *Node) error {
	if n == nil {
		return errors.New("replaceNode() not allowed on a nil node")
	}

	if n == parent.Left {
		parent.Left = replacewith
		return nil
	}
	parent.Right = replacewith
	return nil
}

/*
deleteNode ...
*/
func (n *Node) deleteNode(data int, parent *Node) error {
	if n == nil {
		return errors.New("Value to be deleted does not exist in the tree")
	}

	switch {
	case data < n.Data:
		return n.Left.deleteNode(data, n)
	case data > n.Data:
		return n.Right.deleteNode(data, n)
	default:
		//Node to be deleted is leaf
		if n.Left == nil && n.Right == nil {
			n.replaceNode(parent, nil)
			return nil
		}

		//Node to be deleted has only one child
		if n.Left == nil {
			n.replaceNode(parent, n.Right)
			return nil
		}

		if n.Right == nil {
			n.replaceNode(parent, n.Left)
			return nil
		}

		//Node to be deleted has two children
		succNode := n.successorNode()

		if succNode != nil {
			n.Data = succNode.Data
			return succNode.deleteNode(succNode.Data, n)
		}
	}
	return nil
}

/*
successorNode returns the node in the right subtree that has the minimum value
*/
func (n *Node) successorNode() *Node {

	if n == nil {
		return nil
	}
	succ := n.Right

	// Right subtree has NO left branch, so return the minimum value is the root node of the right subtree
	/* if succ.Left == nil {
		succ = succ.Right
		return succ
	} */
	// Right subtree has left branch, now find min node
	for succ.Left != nil {
		succ = succ.Left
	}

	return succ
}
