package binarysearchtree

import (
	"fmt"
	"reflect"
	"strconv"
	"testing"
)

var bst Tree

func TestInsertTailRecursively(t *testing.T) {
	nums := []int{50, 30, 70, 10, 5, 7, 40, 39, 38, 45, 80, 90, 75}
	for _, val := range nums {
		t.Run(fmt.Sprintf("%s", strconv.Itoa(val)), func(t *testing.T) {
			bst.InsertTailRecursively(val)
		})
	}
	fmt.Printf("Created BST: %v\n", nums)
}

func TestInOrderTraverse(t *testing.T) {
	var tree []int
	var expected = []int{5, 7, 10, 30, 38, 39, 40, 45, 50, 70, 75, 80, 90}
	t.Run("Recursively inserted",
		func(t *testing.T) {
			bst.InOrderTraverse(
				func(data int) {
					tree = append(tree, data)
				})
			fmt.Printf("InOrder Traverse: %v\n", tree)
			if !reflect.DeepEqual(expected, tree) {
				t.Fatalf("Expected in order traverse %v but got %v", expected, tree)
			}
		})
}

func TestSearchNode(t *testing.T) {
	var expected = true
	var searchVal = 90

	t.Run("Search Node", func(t *testing.T) {
		got := bst.SearchNode(searchVal)
		fmt.Printf("Search for data value %d : %t\n", searchVal, got)
		if got != expected {
			t.Fatalf("Expected %t but got %t", expected, got)
		}

		expected = false
		searchVal = 1
		got = bst.SearchNode(searchVal)
		fmt.Printf("Search for data value %d : %t\n", searchVal, got)
		if got != expected {
			t.Fatalf("Expected %t but got %t", expected, got)
		}
	})
}

func BenchmarkInsertTailRecursively(b *testing.B) {
	tree := &Tree{}
	//need to insert big array
	nums := []int{2, 5, 7, 1, 1, 5, 5, 3, 9, 11, 10, 15, 13, 16, 20, 4, 6}
	for _, val := range nums {
		tree.InsertTailRecursively(val)
	}
}

func BenchmarkInsertIteratively(b *testing.B) {
	tree := &Tree{}
	//need to insert big array
	nums := []int{2, 5, 7, 1, 1, 5, 5, 3, 9, 11, 10, 15, 13, 16, 20, 4, 6}
	for _, val := range nums {
		tree.InsertIteratively(val)
	}
}

func TestDeleteNode(t *testing.T) {
	delNums := map[int][]int{
		7:  {5, 10, 30, 38, 39, 40, 45, 50, 70, 75, 80, 90},
		38: {5, 10, 30, 39, 40, 45, 50, 70, 75, 80, 90},
		50: {5, 10, 30, 39, 40, 45, 70, 75, 80, 90},
	}
	var got []int

	for delVal, expected := range delNums {
		t.Run("Search Node", func(t *testing.T) {
			err := bst.DeleteNode(delVal)
			if err != nil {
				t.Fatalf("Node value: %d not deleted\n", delVal)
			}
			got = nil
			bst.InOrderTraverse(
				func(data int) {
					got = append(got, data)
				})
			fmt.Printf("Deleted %d\nInOrder Traverse: %v Expected:%v\n", delVal, got, expected)
			if !reflect.DeepEqual(expected, got) {
				t.Fatalf("Expected in order traverse %v but got %v", expected, got)
			}
		})

	}

}
