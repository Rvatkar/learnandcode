package main

import "fmt"

//TreeNode describes tree structure
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

//PreorderTraversalIterative using stack Root, Left, Right
func PreorderTraversalIterative(root *TreeNode) []int {

	//root,left,right
	if root == nil {
		return nil
	}
	preorder := make([]int, 0)
	stack := make([]*TreeNode, 0)
	currentNode := root
	stack = append(stack, currentNode) //stack push

	for len(stack) > 0 {
		n := len(stack) - 1    // top element
		currentNode = stack[n] //stack top/peek
		stack = stack[:n]      //stack pop
		preorder = append(preorder, currentNode.Val)

		if currentNode.Right != nil {
			stack = append(stack, currentNode.Right) // stack push
		}
		if currentNode.Left != nil {
			stack = append(stack, currentNode.Left) // stack push
		}
		// important note - right child is pushed first so that left child
		// is processed first (LIFO order)
	}
	return preorder
}

//PreorderTraversalRecursive traverse tree recursively and return slice of int
func PreorderTraversalRecursive(root *TreeNode) []int {
	if root == nil {
		return nil
	}

	preorder := make([]int, 0)
	currentNode := root
	preorderRecursive(currentNode, &preorder)
	return preorder
}

func preorderRecursive(root *TreeNode, preorder *[]int) {

	if root != nil {
		*preorder = append(*preorder, root.Val)
		preorderRecursive(root.Left, preorder)
		preorderRecursive(root.Right, preorder)
	}
}

func inorderTraversalRecursive(root *TreeNode) []int {
	if root == nil {
		return nil
	}

	inorder := make([]int, 0)
	currentNode := root
	inorderRecursive(currentNode, &inorder)
	return inorder
}

func inorderRecursive(root *TreeNode, inorder *[]int) {

	if root != nil {
		inorderRecursive(root.Left, inorder)
		*inorder = append(*inorder, root.Val)
		inorderRecursive(root.Right, inorder)
	}
}

func inorderTraversalIterative(root *TreeNode) []int {
	//left,root,right
	if root == nil {
		return nil
	}

	currentNode := root
	stack := make([]*TreeNode, 0)
	inorder := make([]int, 0)

	for len(stack) > 0 || currentNode != nil {
		if currentNode != nil {
			stack = append(stack, currentNode) //push
			currentNode = currentNode.Left
		} else {
			currentNode = stack[len(stack)-1] //top
			stack = stack[:len(stack)-1]      //pop

			inorder = append(inorder, currentNode.Val)
			currentNode = currentNode.Right
		}
	}
	return inorder
}

func isValidBST(root *TreeNode) bool {
	if root == nil {
		return false
	}
	const MaxInt = int(^uint(0) >> 1)
	const MinInt = -MaxInt - 1
	currentNode := root
	return validateBST(currentNode, MinInt, MaxInt)
}

func validateBST(root *TreeNode, min int, max int) bool {
	if root == nil {
		return true
	}
	if root.Val < min || root.Val > max {
		return false
	}

	return validateBST(root.Left, min, root.Val) && validateBST(root.Right, root.Val, max)

}

func insertIntoBSTIterative(root *TreeNode, val int) *TreeNode {
	r := root
	if root == nil {
		return createNode(val)
	}
	for {
		if val > root.Val {
			if root.Right == nil {
				root.Right = createNode(val)
				break
			} else {
				root = root.Right
			}
		} else {
			if root.Left == nil {
				root.Left = createNode(val)
				break
			} else {
				root = root.Left
			}
		}
	}
	return r
}

func createNode(val int) *TreeNode {
	return &TreeNode{
		Val: val,
	}
}

func insertIntoBSTRecursive(root *TreeNode, val int) *TreeNode {
	switch {
	case root == nil:
		root = createNode(val)

	case val < root.Val:
		root.Left = insertIntoBSTRecursive(root.Left, val)

	case val > root.Val:
		root.Right = insertIntoBSTRecursive(root.Right, val)
	}
	return root
}

func searchBST(root *TreeNode, key int) *TreeNode {

	if root == nil {
		return nil
	}

	if root.Val == key {
		return root
	}

	if key < root.Val {
		return searchBST(root.Left, key)
	}
	return searchBST(root.Right, key)

}

func minimumKey(root *TreeNode) *TreeNode {
	for root.Left != nil {
		root = root.Left
	}
	return root
}

func deleteNode(root *TreeNode, key int) *TreeNode {
	if root == nil {
		return root
	}

	if root.Val == key {
		if root.Left == nil && root.Right == nil { // case 1: leaf node i.e, no child
			return nil
		} else if root.Left == nil { // case 2: one child
			return root.Right
		} else if root.Right == nil { // case 2: one child
			return root.Left
		} else { //case 3: two child
			successor := minimumKey(root.Right)
			root.Val = successor.Val
			root.Right = deleteNode(root.Right, successor.Val)
		}
	} else if root.Val > key {
		root.Left = deleteNode(root.Left, key)
	} else {
		root.Right = deleteNode(root.Right, key)
	}

	return root
}

func main() {

	var root *TreeNode
	vals := []int{5, 3, 6, 2, 4, 7}
	for _, val := range vals {
		root = insertIntoBSTRecursive(root, val)
	}

	inorder := inorderTraversalRecursive(root)
	fmt.Println("Inorder BST:")
	for i := range inorder {
		fmt.Printf("%d ", inorder[i])
	}
	fmt.Println()

	key := 3
	searchedNode := searchBST(root, key)
	fmt.Printf("After searching for key: %d  found %+v\n", key, searchedNode)

	deleteKey := 5
	deleteNode(root, deleteKey)

	inorder = inorderTraversalRecursive(root)
	fmt.Println("Inorder BST:")
	for i := range inorder {
		fmt.Printf("%d ", inorder[i])
	}
	fmt.Println()

}
