package quick

import (
	"fmt"
)

//pivotSelection : Choosing the pivot using median-of-three
func pivotSelection(array []int, left int, right int) int {

	center := (left + right) / 2
	fmt.Printf("Unsorted Median-of-three [LEFT][RIGHT][CENTER]: %d %d %d\n", array[left], array[right], array[center])

	//Sort Median-of-three and find the pivot
	if array[center] < array[left] {
		swap(array, left, center)
	}
	if array[right] < array[left] {
		swap(array, left, right)
	}
	if array[right] < array[center] {
		swap(array, center, right)
	}
	//the array will rearrange, get the pivot out of the way - swap it with the next to the last element
	swap(array, center, right-1)
	fmt.Printf("Pivot element:%d\n", array[right-1])

	return array[right-1]
}

// quicksort ...
func quicksort(array []int, left int, right int) {

	/* Partition array must have atleast 3 elements in it.
	If only of three elements in a array, then median of three choice will put them in order.
	so added
	if left+2 <= right {

	}

	If the elements are less than 10, quicksort is not very efficient.
	Instead insertion sort is used at the last phase of sorting.
	if( left + 10 <= right) {

	} else {
		//insertion sort
	}
	*/
	if left+2 <= right {
		/* Note: we know that the first element is smaller than the pivot,
		so the first element to be processed is the element to the right.
		The last two elements are the pivot and an element greater than the pivot,
		so they are not processed.*/
		var i, j int = left + 1, right - 2
		pivot := pivotSelection(array, left, right)

		for {
			/* Why can't use array[++i] or array[--j]? For answer visit https://golang.org/doc/faq#inc_dec*/
			// move the left finger
			for array[i] < pivot {
				i++
			}

			// move the right finger
			for pivot < array[j] {
				j--
			}

			if i < j {
				swap(array, i, j) //swap
				i++
				j--
			} else {
				break // break if fingers have crossed
			}
		}

		swap(array, i, right-1) // restore the pivot
		fmt.Printf("After recursively, array is %d \n", array)

		quicksort(array, left, i-1)  // call quicksort for the left part
		quicksort(array, i+1, right) // call quicksort for the right part
	}
}

//swap - swap index1 element with index2 element
func swap(unsorted []int, index1 int, index2 int) {
	temp := unsorted[index1]
	unsorted[index1] = unsorted[index2]
	unsorted[index2] = temp
}

// Start ...
func Start() {

	//unsorted := []int{10, 3, 25, 6, 8, 17, 1, 2, 18, 5}
	unsorted := []int{10, 1, 20, 3, 30, 4, 50, 5, 60, 7, 80, 80, 6, 90, 8, 100, 10, 12, 44, 33, 1000, 22, 40}

	fmt.Printf("Unsorted array : %d\n", unsorted)

	left := 0                  // zeroth element of array
	right := len(unsorted) - 1 //last element index
	quicksort(unsorted, left, right)

	fmt.Printf("Quicksort Array: %d\n", unsorted)
}
