package merge

import (
	"fmt"
)

func merge(array []int, left int, mid int, right int) {

	//stores the starting position of both parts in temporary variables.
	i := left
	j := mid + 1

	var temp []int
	for k := left; k <= right; k++ {
		//checks if first part comes to an end or not
		if i > mid {
			temp = append(temp, array[j])
			j++
		} else if j > right { //checks if second part comes to an end or not
			temp = append(temp, array[i])
			i++
		} else if array[i] < array[j] { //checks which part has smaller element
			temp = append(temp, array[i])
			i++
		} else {
			temp = append(temp, array[j])
			j++
		}
	}

	tempLen := len(temp)
	/* Now the real array has elements in sorted manner including both parts.*/
	for i := 0; i < tempLen; i++ {
		array[left] = temp[i]
		left++
	}

}

func sort(array []int, left int, right int) {

	/* Switching to insertion sort for small subarrays will improve the running time of a
	typical mergesort implementation by 10 to 15 percent.

	if right <= left + 7) {
		//INSERTION SORT
		return;
	}
	*/
	if left < right {
		mid := left + (right-left)/2
		sort(array, left, mid)
		sort(array, mid+1, right)
		merge(array, left, mid, right)
	}
	return
}

//Start ...
func Start() {
	unsorted := []int{10, 3, 25, 6, 8, 17, 1, 2, 18, 5}
	fmt.Printf("Unsorted array : %d\n", unsorted)
	sort(unsorted, 0, len(unsorted)-1)
	fmt.Printf("Sorted array: %d\n", unsorted)
}
