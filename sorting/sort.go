package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/Rvatkar/learnandcode/sorting/insertion"
	"bitbucket.org/Rvatkar/learnandcode/sorting/merge"
	"bitbucket.org/Rvatkar/learnandcode/sorting/quick"
)

func main() {
	fmt.Println("Sorting Algorithms ( Select sort )")
	sortNames := `
	1. Quick Sort
	2. Merge Sort
	3. Bubble Sort
	4. Insertion Sort
	5. Selection Sort
	6. Shell Sort
	7. Bucket Sort
	8. Heap Sort
	9. Exit`

	fmt.Printf("%s\n", sortNames)
	input := bufio.NewScanner(os.Stdin)
	for {
		input.Scan()
		option, err := strconv.Atoi(input.Text())
		if err != nil {
			fmt.Printf("Entered value is not numeric: %s\n", input.Text())
		}
		switch option {
		case 1:
			fmt.Println("Quick Sort")
			quick.Start()
		case 2:
			fmt.Println("Merge Sort")
			merge.Start()
		case 3:
			fmt.Println("Bubble Sort")
		case 4:
			fmt.Println("Insertion Sort")
			insertion.Start()
		case 5:
			fmt.Println("Selection Sort")
		case 6:
			fmt.Println("Shell Sort")
		case 7:
			fmt.Println("Bucket Sort")
		case 8:
			fmt.Println("Heap Sort")
		case 9:
			os.Exit(1)
		default:
			fmt.Println("Please select option from menu")
		}

	}
}
