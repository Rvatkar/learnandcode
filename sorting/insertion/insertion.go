package insertion

import (
	"fmt"
)

func insertion(array []int) {
	arrayLen := len(array)

	for i := 1; i < arrayLen; i++ {
		comparable := array[i]
		j := i
		for j > 0 && comparable < array[j-1] {
			array[j] = array[j-1]
			j = j - 1
		}

		// Place comparable at after the element just smaller than it.
		array[j] = comparable
	}
}

//Start ...
func Start() {
	unsorted := []int{9, 5, 1, 4, 3, 7, 2}
	fmt.Printf("Unsorted array : %d\n", unsorted)
	insertion(unsorted)
	fmt.Printf("Sorted array: %d\n", unsorted)
}
