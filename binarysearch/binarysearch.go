package main

import (
	"fmt"
)

func binarysearch_iterative(haystack []int, needle int) bool {

	// edge cases
	if haystack[0] == needle {
		return true
	}
	high := len(haystack) - 1
	if haystack[high] == needle {
		return true
	}
	// As we already checked index 0th and last index. 
	// we can start checking from index 1 to last index minus one
	low := 1
	high = high - 1

	for low <= high {
		median := int(low + (high - low) / 2)
		medianValue := haystack[median]

		if medianValue == needle {
			return true
		} else if medianValue < needle {
			low = median + 1
		} else {
			high = median -1
		}
	}
	return false
}

func main()  {
	sortedHaystack := []int {1,4,6,10,20,23,45,67,77,78,80,82,90,98,100}
	searchValue := 67
	if binarysearch_iterative(sortedHaystack, searchValue) {
		fmt.Printf("Search value %d found\n", searchValue)
	} else {
		fmt.Printf("Search value %d not found\n", searchValue)
	}
}

/*
The major difference between the iterative and recursive version of Binary Search is 
that the recursive version has a space complexity of O(log N) while the iterative version 
has a space complexity of O(1). Hence, even though recursive version may be easy to 
implement, the iterative version is efficient.

Input data needs to be sorted in Binary Search and not in Linear Search.
*/