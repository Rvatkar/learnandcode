package main

import (
	"fmt"
	"time"
)

func fanin(input1, input2 <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		for {
			select {
			case s := <-input1:
				out <- s
			case s := <-input2:
				out <- s
			/* The time.After function returns a channel that blocks for the specified duration.
			After the interval, the channel delivers the current time, once.*/
			case <-time.After(1 * time.Second):
				fmt.Println("You're too slow to compute square")
				return
			}
		}
	}()
	return out
}

func main() {

	square := func(num int) <-chan int {
		out := make(chan int)
		go func() {
			out <- num * num
			close(out)
		}()

		return out
	}

	c1 := square(2)
	c2 := square(4)

	// Consume the first value from output.
	c := fanin(c1, c2)
	fmt.Printf("First to process: %d\n", <-c)

}
