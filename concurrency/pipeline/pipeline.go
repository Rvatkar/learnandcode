package main

import (
	"fmt"
	"sync"
)

/* https://blog.golang.org/pipelines */

/*The first stage, gen, is a function that converts a list of integers to a channel that emits
the integers in the list. The gen function starts a goroutine that sends the integers on the channel
and closes the channel when all the values have been sent.*/
func generator(nums ...int) <-chan int {
	outbound := make(chan int)
	fmt.Println(nums)
	go func() {
		for _, n := range nums {
			outbound <- n
		}
		close(outbound)
	}()

	return outbound
}

/*The second stage, sq, receives integers from a channel and returns a channel that emits
the square of each received integer. After the inbound channel is closed and this stage has sent all
the values downstream, it closes the outbound channel*/
func square(inbound <-chan int) <-chan int {
	outbound := make(chan int)
	go func() {
		for n := range inbound {
			outbound <- n * n
		}
		close(outbound)
	}()
	return outbound
}

func merge(cs ...<-chan int) <-chan int {
	var wg sync.WaitGroup
	out := make(chan int)

	// Start an output goroutine for each input channel in cs.  output
	// copies values from c to out until c is closed, then calls wg.Done.
	output := func(c <-chan int) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}

	wg.Add(len(cs))

	for _, c := range cs {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}

func main() {
	/* TRY ALL THE OPTIONS */
	/*The main function sets up the pipeline and runs the final stage: it receives values from
	the second stage and prints each one, until the channel is closed*/

	/*// Set up the pipeline.
	s := generator(2, 3)
	nums := []int{2, 3, 4}
	s1 := generator(nums...)
	out := square(s)
	out1 := square(s1)

	// Consume the output
	fmt.Println(<-out)
	fmt.Println(<-out)
	fmt.Println("S1")
	fmt.Println(<-out1)
	fmt.Println(<-out1)
	fmt.Println(<-out1)
	fmt.Println(<-out1)*/

	/*=====================================================================================*/

	/*Since sq has the same type for its inbound and outbound channels, we can compose it any
	number of times. We can also rewrite main as a range loop, like the other stages*/

	/*for n := range square(square(generator(2, 3))) {
		fmt.Println(n)
	}*/

	/*=====================================================================================*/

	/* Fan-out, fan-in
	Multiple functions can read from the same channel until that channel is closed;
	this is called fan-out. This provides a way to distribute work amongst a group of workers to
	parallelize CPU use and I/O.

	A function can read from multiple inputs and proceed until all are closed by multiplexing the
	input channels onto a single channel that's closed when all the inputs are closed.
	This is called fan-in.

	We can change our pipeline to run two instances of sq, each reading from the same input channel.
	We introduce a new function, merge, to fan in the results
	*/

	inbound := generator(3, 4)

	// Distribute the sq work across two goroutines that both read from in.
	c1 := square(inbound)
	c2 := square(inbound)

	// Consume the merged output from c1 and c2.
	for n := range merge(c1, c2) {
		fmt.Println(n)
	}
}
