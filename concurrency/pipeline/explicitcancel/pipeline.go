package main

/*
Here are the guidelines for pipeline construction:

1) stages close their outbound channels when all the send operations are done.
2) stages keep receiving values from inbound channels until those channels are closed or
the senders are unblocked.

Pipelines unblock senders either by ensuring there's enough buffer for all the values that are sent
or by explicitly signalling senders when the receiver may abandon the channel

*/

import (
	"fmt"
	"sync"
)

func generator(done <-chan struct{}, nums ...int) <-chan int {

	outbound := make(chan int)
	fmt.Println(nums)
	go func() {
		defer close(outbound)
		for _, n := range nums {
			select {
			case outbound <- n:
			case <-done:
				return
			}
		}
	}()
	return outbound
}

/*
square can return as soon as done is closed. square ensures its out channel is closed on all
return paths via a defer statement
*/

func square(done <-chan struct{}, inbound <-chan int) <-chan int {
	outbound := make(chan int)
	go func() {
		defer close(outbound)
		for n := range inbound {
			select {
			case outbound <- n * n:
			case <-done:
				return
			}
		}
	}()
	return outbound
}

/*
Each of our pipeline stages is now free to return as soon as done is closed. The output routine in
merge can return without draining its inbound channel, since it knows the upstream sender,
sq, will stop attempting to send when done is closed. output ensures wg.Done is called on all return paths via a defer statement
*/

func merge(done <-chan struct{}, cs ...<-chan int) <-chan int {
	var wg sync.WaitGroup
	outbound := make(chan int)

	output := func(c <-chan int) {
		defer wg.Done()
		for n := range c {
			select {
			case outbound <- n:
			case <-done:
				return
			}
		}
	}

	wg.Add(len(cs))

	for _, c := range cs {
		go output(c)
	}

	go func() {
		wg.Wait()
		close(outbound)
	}()

	return outbound
}

/*
We need a way to tell an unknown and unbounded number of goroutines to stop sending their values
downstream. In Go, we can do this by closing a channel, because a receive operation on a closed channel
can always proceed immediately, yielding the element type's zero value.

This means that main can unblock all the senders simply by closing the done channel.
This close is effectively a broadcast signal to the senders. We extend each of our pipeline functions
to accept done as a parameter and arrange for the close to happen via a defer statement,
so that all return paths from main will signal the pipeline stages to exit
*/

func main() {
	// Set up a done channel that's shared by the whole pipeline,
	// and close that channel when this pipeline exits, as a signal
	// for all the goroutines we started to exit.
	done := make(chan struct{})
	defer close(done)

	in := generator(done, 4, 5)

	// Distribute the sq work across two goroutines that both read from in.
	c1 := square(done, in)
	c2 := square(done, in)

	// Consume the first value from output.
	/*out := merge(done, c1, c2)
	fmt.Println(<-out) // 4 or 9*/
	//OR use for loop to get
	for n := range merge(done, c1, c2) {
		fmt.Println(n)
	}

	// done will be closed by the deferred call.

}
