package leetcode

type Node struct {
	val  int
	next *Node
}

type MyLinkedList struct {
	head *Node
}

func checkIndex(index int) bool {
	return index >= 0 && index <= 1000
}
func checkValue(val int) bool {
	return val >= 0 && val <= 1000
}

/** Initialize your data structure here. */
func Constructor() MyLinkedList {
	return MyLinkedList{}
}

/** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
func (this *MyLinkedList) Get(index int) int {

	head := this.head
	if head == nil || !checkIndex(index) {
		return -1
	}
	if index == 0 {
		return head.val
	}
	listIndex := 0
	for head != nil {
		if listIndex == index {
			return head.val
		}
		head = head.next
		listIndex++
	}
	return -1
}

/** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
func (this *MyLinkedList) AddAtHead(val int) {
	if checkValue(val) {
		newNode := &Node{}
		newNode.val = val
		newNode.next = this.head

		this.head = newNode
	}
}

/** Append a node of value val to the last element of the linked list. */
func (this *MyLinkedList) AddAtTail(val int) {
	if checkValue(val) {

		current_node := this.head
		for current_node.next != nil {
			current_node = current_node.next
		}

		new_node := &Node{val: val}
		current_node.next = new_node
	}
}

/** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
func (this *MyLinkedList) AddAtIndex(index int, val int) {
	if checkValue(val) && checkIndex(index) {
		if index == 0 {
			this.AddAtHead(val)
		}
		head := this.head
		listLength := 0

		for head != nil {
			if listLength == index-1 {
				prev_node := head
				new_node := &Node{val: val}
				new_node.next = prev_node.next
				prev_node.next = new_node
				break
			}
			listLength++
			head = head.next
		}
	}
}

/** Delete the index-th node in the linked list, if the index is valid. */
func (this *MyLinkedList) DeleteAtIndex(index int) {

	if !checkIndex(index) || this.head == nil {
		return
	}

	listIndex := 0
	if index == listIndex {
		this.head = this.head.next
		return
	}

	head := this.head
	for head.next != nil {
		if listIndex == index-1 {
			head.next = head.next.next
			return
		}
		listIndex++
		head = head.next
	}

}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Get(index);
 * obj.AddAtHead(val);
 * obj.AddAtTail(val);
 * obj.AddAtIndex(index,val);
 * obj.DeleteAtIndex(index);
 */

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
/* func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}
	var temp, reverseNode *ListNode
	for head != nil {
		reverseNode = head
		head = head.Next
		reverseNode.Next = temp
		temp = reverseNode
	}
	head = reverseNode
	return head
} */
