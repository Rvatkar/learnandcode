package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

//Node - node to hold data and address
type Node struct {
	data int64
	next *Node
}

//List  - linked list
type List struct {
	head *Node
}

//insert - insert one more Node in list
func (l *List) insert(number int64) {
	/* 1. allocate node and put in the data.
	Variables declared without an explicit initial value are given their zero value.
	So for pointer 'next' value will be nil
	*/
	newNode := &Node{data: number}

	/* If the Linked List is empty, then make the new node as head */
	if l.head == nil {
		l.head = newNode
		return
	}

	/* Store head node in current node and iterate till tail of list */
	currentNode := l.head
	for currentNode.next != nil {
		currentNode = currentNode.next
	}
	/* append new node to tail of list */
	currentNode.next = newNode

}

//traverse - display all linked list
func (l *List) traverse() {

	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	fmt.Println("Singly Linked List:")
	//traversal list
	list := l.head
	for list != nil {
		fmt.Printf("%d -> ", list.data)
		list = list.next
	}
	fmt.Println("")
}

//insertAtPosition - insert node at given position
func (l *List) insertAtPosition(position int, number int64) {
	//Check if position exists
	fmt.Printf("Length OF LIST: %d\n", l.getLength())
	position = func() int {
		if position == 0 || position < 0 {
			return 0
		}
		return position //- 1
	}()

	listLength := l.getLength()
	switch {
	case position > listLength:
		fmt.Println("Entered position exceed list length")
		return
	case position == 0:
		fmt.Println("At the beginning of the list")

		//At the front of the linked list
		/* 1. allocate node */
		newNode := &Node{}

		/* 2. put in the data  */
		newNode.data = number

		/* 3. Make next of new node as head */
		newNode.next = l.head

		/* 4. move the head to point to the new node */
		l.head = newNode

	case position == listLength:
		//At the end of the linked list
		fmt.Println("At end of the list")

		l.insert(number)

	case position <= listLength:
		fmt.Println("At given position")
		//After a given node
		start := l.head
		counter := 0
		prevNode := &Node{}
		newNode := &Node{data: number}
		for start != nil {
			// position -1 : for getting previous node info
			if (position - 1) == counter {
				prevNode = start
				newNode.next = prevNode.next
				prevNode.next = newNode
			}
			start = start.next
			counter++
		}
	}
}

func (l *List) insertBeforeValue(value int64, number int64) {
	start := l.head
	position := 0
	for start != nil {
		if start.data == value {
			break
		}
		start = start.next
		position++

	}
	fmt.Printf("BEFORE: VALUE:%d POSITION:%d", value, position)
	l.insertAtPosition(position, number)

}

func (l *List) insertAfterValue(value int64, number int64) {
	start := l.head
	position := 0
	for start != nil {
		position++
		if start.data == value {
			break
		}
		start = start.next
	}

	fmt.Printf("AFTER: VALUE:%d POSITION:%d", value, position)

	l.insertAtPosition(position, number)
}

//deleteByValue - delete node at given position
func (l *List) deleteByValue(number int64) {

	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	//To delete head node
	if l.head.data == number {
		// Delete head by overwriting it with next node
		l.head = l.head.next
		return
	}

	//To delete given node
	currentNode := l.head
	for currentNode != nil {
		if currentNode.next.data == number {
			currentNode.next = currentNode.next.next
			return
		}
		currentNode = currentNode.next
	}
}

func (l *List) getLength() int {
	if l.head == nil {
		return 0
	}

	counter := 0
	start := l.head
	for start != nil {
		start = start.next
		counter++
	}

	return counter
}

//reverse - reverse the list
func (l *List) reverse() {
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	// Initialize current, previous and
	// next pointers
	currentNode := l.head
	prevNode := &Node{}
	nextNode := &Node{}

	headFlag := true

	for currentNode != nil {
		// Store next
		nextNode = currentNode.next

		if headFlag {
			//check if node is head and assign head next to nil. can't assign prevNode, as it stores zero value of node
			currentNode.next = nil
		} else {
			// Reverse current node's pointer
			currentNode.next = prevNode
		}

		// Move pointers one position ahead.
		prevNode = currentNode
		currentNode = nextNode
		headFlag = false
	}
	l.head = prevNode
}

//main - Command line based singly linked list
func main() {
	fmt.Println("Singly Linked List")

	//initialized head node to nil
	list := &List{}

	input := bufio.NewScanner(os.Stdin)
	for {
		fmt.Println("Select Main Option: INSERT (Create a list using number(s) )/APPENDP (By Position)/APPENDV (By Value)/DELETE (By value)/TRAVERSE/REVERSE/EXIT)")
		input.Scan()

		switch input.Text() {
		case "INSERT":
			fmt.Println("Enter number (EXIT - to exit from ADD function):")
			addNumber := make([]int64, 0)
			for {
				input.Scan()
				number := input.Text()
				if len(number) == 0 || number == "EXIT" {
					break
				}

				//convert string to int64
				int64Number, err := strconv.ParseInt(number, 10, 64)
				if err != nil {
					fmt.Printf("Entered value is not numeric: %s\n", number)
				}
				addNumber = append(addNumber, int64Number)
			}
			for _, number := range addNumber {
				list.insert(number)
			}

			list.traverse()

		case "DELETE":
			fmt.Println("Enter number:")
			input.Scan()
			value, err := strconv.ParseInt(input.Text(), 10, 64)
			if err != nil {
				fmt.Printf("Entered value is not numeric: %s\n", input.Text())
				break
			}

			list.deleteByValue(value)
			list.traverse()

		case "APPENDP":
			fmt.Println("Enter position:")
			input.Scan()
			positionAt, err := strconv.Atoi(input.Text())
			if err != nil {
				fmt.Printf("Entered value is not numeric: %s\n", input.Text())
			}

			fmt.Println("Enter number:")
			input.Scan()
			number, err := strconv.ParseInt(input.Text(), 10, 64)
			if err != nil {
				fmt.Printf("Entered value is not numeric: %s\n", input.Text())
			}
			list.insertAtPosition(positionAt, number)
			list.traverse()

		case "APPENDV":
			fmt.Println("Select (BEFORE / AFTER):")
			input.Scan()
			appendPosition := input.Text()

			fmt.Println("Select number:")
			input.Scan()
			valueAt, err := strconv.ParseInt(input.Text(), 10, 64)
			if err != nil {
				fmt.Printf("Entered value is not numeric: %s\n", input.Text())
			}

			fmt.Printf("Enter number to %s:", appendPosition)
			input.Scan()
			newNumber, err := strconv.ParseInt(input.Text(), 10, 64)
			if err != nil {
				fmt.Printf("Entered value is not numeric: %s\n", input.Text())
			}

			switch appendPosition {
			case "BEFORE":
				list.insertBeforeValue(valueAt, newNumber)
			case "AFTER":
				list.insertAfterValue(valueAt, newNumber)
			default:
				break
			}

			list.traverse()

		case "TRAVERSE":
			list.traverse()
		case "REVERSE":
			list.reverse()
			list.traverse()
		case "EXIT":
			os.Exit(1)
		default:
			fmt.Println("Please select option from menu")
		}
	}
}
