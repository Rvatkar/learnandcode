package main

import (
	"fmt"
)

type node struct {
	data int
	next *node
}

//stack - LIFO or FILO
type stack struct {
	top *node
}

// push : Adds an item in the stack.
func (s *stack) push(value int) {

	s.top = &node{
		data: value,
		next: s.top,
	}
	fmt.Printf("%d pushed to stack [ %+v ] \n", value, s.top)
}

//pop : Removes an item from the stack.
func (s *stack) pop() {
	if s.isEmpty() {
		fmt.Println("Stack is empty")
		return
	}

	fmt.Printf("%d popped from stack\n", s.top.data)
	s.top = s.top.next

}

//peek : Display top element of stack.
func (s *stack) peek() {
	if s.isEmpty() {
		fmt.Println("Stack is empty")
		return
	}

	fmt.Printf("Top node is : %+v\n", s.top)
}

func (s *stack) isEmpty() bool {

	if s.top != nil {
		return false
	}
	return true
}

func main() {
	stackTop := &stack{}

	stackTop.push(10)
	stackTop.push(20)
	stackTop.push(30)

	stackTop.pop()
	stackTop.peek()

	stackTop.pop()
	stackTop.peek()

	stackTop.pop()
	stackTop.peek()

}
