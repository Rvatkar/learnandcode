package main

import (
	"strings"
	"fmt"
)

func reverse_sentence( s string) string {
	sLen := len(s) - 1
	var sb strings.Builder
	for i:= sLen; i >= 0; i-- {
		sb.WriteString(s[i:i+1])
	}
	return sb.String()
}

func reverse_words(s string) string {
	sLen := len(s) - 1
	fmt.Printf("Reverse sentence: %s\nLength of sentence: %d\n", s,sLen)
	var sb strings.Builder
	var reverse strings.Builder
	for i := 0; i <= sLen; i++ {
		sb.WriteString(s[i:i+1])
		if s[i:i+1] == " " || i == sLen {
			word := sb.String()
			sb.Reset()
			wordLen := len(word)
			if i != sLen {
				wordLen = wordLen - 1
			}
			fmt.Printf("%s:%d\n",word, wordLen)
			for j := wordLen; j > 0; j-- {
				reverse.WriteString(word[j-1:j])
			}
			if i != sLen {
				reverse.WriteString(s[i:i+1])
			}
		}
	}
	return reverse.String()
}

func main()  {
	reverseStr := reverse_sentence("ram is costly") 
	//fmt.Println(reverseStr)
	reverseWords:= reverse_words(reverseStr)
	fmt.Println(reverseWords)
}