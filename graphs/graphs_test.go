package graphs

import (
	"fmt"
	"testing"
)

/*
Test graph

0 ------ 1
|	   / |	\
|	  /	 |	 \
|   /	 |	  2
|  /	 |	 /
| /		 |  /
4 ------ 3

*/

var g Graph

func createGraph() {
	zero := Node{0}
	one := Node{1}
	two := Node{2}
	three := Node{3}
	four := Node{4}

	g.AddNode(&zero)
	g.AddNode(&one)
	g.AddNode(&two)
	g.AddNode(&three)
	g.AddNode(&four)

	g.AddEdges(&zero, &one)
	g.AddEdges(&zero, &four)
	g.AddEdges(&one, &four)
	g.AddEdges(&one, &three)
	g.AddEdges(&one, &two)
	g.AddEdges(&two, &three)
	g.AddEdges(&three, &four)
}

func TestAdd(t *testing.T) {
	createGraph()
	g.PrintGraph()
}

func TestBFSTraversal(t *testing.T) {
	g.BFSTraversal(func(n *Node) {
		fmt.Printf("%v\n", *n)
	})
}
