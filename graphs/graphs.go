package graphs

import "fmt"

//Graph to hold Adjacency List
type Graph struct {
	vertices []*Node
	edges    map[Node][]*Node //https://blog.golang.org/maps
}

//Node is also refer vertex. Node holds its data value
type Node struct {
	data int
}

//AddNode creates a new vertex in a graph
func (g *Graph) AddNode(n *Node) {
	g.vertices = append(g.vertices, n)
}

//AddEdges creates the edges among vertices. It is undirected graph
func (g *Graph) AddEdges(n1, n2 *Node) {
	if g.edges == nil {
		g.edges = make(map[Node][]*Node)
	}

	g.edges[*n1] = append(g.edges[*n1], n2)
	g.edges[*n2] = append(g.edges[*n2], n1)
}

//PrintGraph prints create graph
func (g *Graph) PrintGraph() {

	if g.vertices != nil {
		noOfVertices := len(g.vertices)
		fmt.Printf("Number of vertices in graph: %d\n", noOfVertices)
		for i := 0; i < noOfVertices; i++ {
			fmt.Printf("Vertex %d -> Edges{ ", *g.vertices[i])
			noOfEdges := len(g.edges[*g.vertices[i]])
			for j := 0; j < noOfEdges; j++ {
				fmt.Printf("%d ", *g.edges[*g.vertices[i]][j])
			}
			fmt.Println("}")
		}
	}
}

/*
Queue for BSF
*/
//QueueNode to store verices in queue
type QueueNode struct {
	vertex []Node
}

func (q *QueueNode) enqueue(n Node) {

	if len(q.vertex) == 0 {
		//Create new slice of nodes
		q.vertex = []Node{}
	}

	//Add data into queue
	q.vertex = append(q.vertex, n)
}

func (q *QueueNode) dequeue() *Node {
	if len(q.vertex) == 0 {
		fmt.Println("Queue is empty")
		return nil
	}

	//Front node to dequeue
	frontNode := q.vertex[0]

	//update queue
	q.vertex = q.vertex[1:len(q.vertex)]

	return &frontNode
}

/*
BFS algorithm

A standard BFS implementation puts each vertex of the graph into one of two categories:

1. Visited
2. Not Visited

The purpose of the algorithm is to mark each vertex as visited while avoiding cycles.

The algorithm works as follows:

1. Start by putting any one of the graph's vertices at the back of a queue.
2. Take the front item of the queue and add it to the visited list.
3. Create a list of that vertex's adjacent nodes. Add the ones which aren't in the visited list to the back of the queue.
4. Keep repeating steps 2 and 3 until the queue is empty.
*/
//BFSTraversal ...
func (g *Graph) BFSTraversal(f func(*Node)) {
	//create new queue
	q := QueueNode{}
	q.vertex = []Node{}

	//get index node from graph for traverse
	vertex := g.vertices[2]

	//Put index node into queue
	q.enqueue(*vertex)

	//Create visited list
	visited := make(map[*Node]bool)

	for len(q.vertex) != 0 {

		//get first node from queue
		node := q.dequeue()

		//put it into visited list
		visited[node] = true

		//get all associated edges of node and loop through node's edges
		edges := g.edges[*node]

		for i := 0; i < len(edges); i++ {

			edge := edges[i]

			//check edge is visited
			if !visited[edge] {

				//add into queue
				q.enqueue(*edge)

				//mark as visited
				visited[edge] = true
			}
		}

		if f != nil {
			f(node) //to print node
		}
	}
}
