package dynamic

import (
	"testing"
)

func TestFibonacciTabulation(t *testing.T) {
	nums := []int{0, 1, 5, 10, 50}
	expected := []int64{0, 1, 5, 55, 12586269025}

	t.Run("Tabulation",
		func(t *testing.T) {
			for i, num := range nums {
				result := FibonacciTabulation(num)
				if result != expected[i] {
					t.Errorf("Expected %d for passed value %d\n", expected[i], num)
				}
			}
		})
}

func TestFibonacciMemoization(t *testing.T) {
	nums := []int{0, 1, 5, 10, 50}
	expected := []int64{0, 1, 5, 55, 12586269025}

	t.Run("Memoization",
		func(t *testing.T) {
			for i, num := range nums {
				InitMemoization(num)
				result := FibonacciMemoization(num)
				if result != expected[i] {
					t.Errorf("Expected %d for passed value %d received %d\n", expected[i], num, result)
				}
			}
		})
}
