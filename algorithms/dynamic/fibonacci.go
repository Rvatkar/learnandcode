package dynamic

var fibMemo []int64

//FibonacciTabulation : solving using Tabulation/ Bottom-up method
func FibonacciTabulation(num int) int64 {
	if num < 1 {
		return int64(num)
	}

	fibNumbers := make([]int64, num+1)
	fibNumbers[0] = 0
	fibNumbers[1] = 1

	for i := 2; i <= num; i++ {
		fibNumbers[i] = fibNumbers[i-1] + fibNumbers[i-2]
	}
	return fibNumbers[num]
}

//FibonacciMemoization : solving using Memoization / Top-Bottom method
func FibonacciMemoization(num int) int64 {

	if num <= 1 {
		return int64(num)
	}
	if fibMemo[num] != -1 {
		return fibMemo[num]
	}
	fibMemo[num] = FibonacciMemoization(num-1) + FibonacciMemoization(num-2)
	return fibMemo[num]
}

//InitMemoization initialized array to save fib result
func InitMemoization(num int) {
	if num < 0 {
		return
	}
	fibMemo = make([]int64, num+1)
	for i := 0; i <= num; i++ {
		fibMemo[i] = -1
	}
}
