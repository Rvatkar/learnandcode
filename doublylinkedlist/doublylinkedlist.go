package main

import (
	"fmt"
)

type dllnode struct {
	data int
	next *dllnode
	prev *dllnode
}

type list struct {
	head *dllnode
}

func (l *list) createlist(value int) {

	newNode := &dllnode{data: value}

	/* If the Linked List is empty, then make the new node as head */
	if l.head == nil {
		l.head = newNode
		return
	}

	currentNode := l.head
	//move till end of the list
	for currentNode.next != nil {
		currentNode = currentNode.next
	}

	newNode.prev = currentNode
	currentNode.next = newNode

}
func (l *list) insertAtEnd(value int) {
	fmt.Printf("INSERTED AT END OF THE LIST, NUMBER: %d\n", value)
	l.createlist(value)
}

func (l *list) insertAtFront(value int) {
	fmt.Printf("INSERTED AT FRONT OF THE LIST, NUMBER: %d\n", value)
	/* 1. allocate node
	 * 2. put in the data */
	newNode := &dllnode{data: value}

	/* If the Linked List is empty, then make the new node as head */
	if l.head == nil {
		l.head = newNode
		return
	}

	/* 3. Make next of new node as head */
	newNode.next = l.head
	/* 4. change prev of head node to new node */
	l.head.prev = newNode
	/* 5. move the head to point to the new node */
	l.head = newNode

}

func (l *list) insertAtGivenPosition(position int, value int) {
	fmt.Printf("INSERTED NUMBER: %d AT GIVEN POSITION: %d\n", value, position)

	if position == 0 {
		l.insertAtFront(value)
		return
	}
	counter := 0
	currentNode := l.head
	for currentNode != nil {
		if position == counter {
			newNode := &dllnode{data: value}

			newNode.prev = currentNode.prev
			currentNode.prev.next = newNode
			newNode.next = currentNode
			currentNode.prev = newNode
		}
		currentNode = currentNode.next
		counter++
	}

	if position >= counter {
		l.insertAtEnd(value)
	}
}

func (l *list) insertBeforeNode(beforeValue int, value int) {
	fmt.Printf("INSERTED NUMBER: %d BEFORE NODE DATA: %d\n", value, beforeValue)
	position := 0

	currentNode := l.head
	for currentNode != nil {
		if currentNode.data == beforeValue {
			break
		}
		currentNode = currentNode.next
		position++
	}
	fmt.Printf("PASSED POSITION:%d\n", position)
	l.insertAtGivenPosition(position, value)

}

func (l *list) insertAfterNode(afterValue int, value int) {
	fmt.Printf("INSERTED NUMBER: %d AFTER NODE DATA: %d\n", value, afterValue)

	position := 0

	currentNode := l.head
	for currentNode != nil {
		position++
		if afterValue == currentNode.data {
			break
		}
		currentNode = currentNode.next
	}
	fmt.Printf("PASSED POSITION:%d\n", position)
	l.insertAtGivenPosition(position, value)

}

func (l *list) deleteEndNode() {
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	currentNode := l.head
	for currentNode != nil {
		if currentNode.next == nil {
			fmt.Printf("DELETED END NODE NUMBER: %d\n", currentNode.data)
			currentNode.prev.next = nil
			currentNode.prev = nil
		}
		currentNode = currentNode.next
	}
}

func (l *list) deleteFrontNode() {
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}
	fmt.Printf("DELETED FRONT NODE NUMBER: %d\n", l.head.data)
	oldHead := l.head
	newHead := l.head.next

	newHead.prev = nil
	oldHead.next = nil

	l.head = newHead
}

func (l *list) deleteNodeByPosition(position int) {
	fmt.Println("DELETE NODE BY POSITION")
	if position == 0 {
		l.deleteFrontNode()
		return
	}
	listLength := l.getListLength()

	if position == listLength {
		l.deleteEndNode()
		return
	}

	counter := 0
	currentNode := l.head
	for currentNode != nil {
		if position == counter {
			fmt.Printf("DELETED NUMBER: %d AT POSITION: %d\n", currentNode.data, position)
			prevNode := currentNode.prev
			nextNode := currentNode.next

			prevNode.next = nextNode
			nextNode.prev = prevNode
			currentNode = prevNode
		}
		currentNode = currentNode.next
		counter++
	}
}

func (l *list) deleteBeforeNode(beforeVale int) {
	fmt.Printf("DELETED NODE BEFORE NUMBER: %d\n", beforeVale)
	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}
	position := 0

	currentNode := l.head
	for currentNode != nil {
		if currentNode.data == beforeVale {
			break
		}
		currentNode = currentNode.next
		position++
	}

	l.deleteNodeByPosition(position - 1)

}

func (l *list) deleteAfterNode(afterValue int) {
	fmt.Printf("DELETED NODE AFTER NUMBER: %d\n", afterValue)
	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}
	position := 0

	currentNode := l.head
	for currentNode != nil {
		position++
		if currentNode.data == afterValue {
			break
		}
		currentNode = currentNode.next
	}

	l.deleteNodeByPosition(position)

}

func (l *list) getListLength() int {
	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return 0
	}
	counter := 0
	currentNode := l.head
	for currentNode != nil {
		currentNode = currentNode.next
		counter++
	}
	return counter - 1
}

func (l *list) reverse() {
	fmt.Println("REVERSE OF DOUBLY LINKED LIST")

}

func (l *list) traverse() {
	//Check if list is empty
	if l.head == nil {
		fmt.Println("List is empty")
		return
	}

	currentNode := l.head
	for currentNode != nil {
		//fmt.Printf("<-(%v) %d (%v) ->\n", currentNode.prev, currentNode.data, currentNode.next)
		fmt.Printf("<-%d->", currentNode.data)
		currentNode = currentNode.next
	}
	fmt.Println("")

}

func main() {
	// first import github.com/pkg/profile
	//profileFileDir := "/learnandcode/profile" //absolute path to folder
	//defer profile.Start(profile.MemProfile, profile.ProfilePath(profileFileDir),profile.NoShutdownHook).Stop()

	list := &list{}

	fmt.Println("CREATED LIST")
	for i := 1; i <= 10; i++ {
		list.createlist(i)
	}
	list.traverse()

	list.insertAtEnd(100)
	list.traverse()

	list.insertAtFront(200)
	list.traverse()

	list.insertAtGivenPosition(2, 300)
	list.traverse()

	list.insertAtGivenPosition(0, 400)
	list.traverse()

	list.insertAtGivenPosition(14, 700)
	list.traverse()

	list.insertBeforeNode(5, 500)
	list.traverse()

	list.insertBeforeNode(400, 40)
	list.traverse()

	list.insertBeforeNode(700, 70)
	list.traverse()

	list.insertAfterNode(40, 4)
	list.traverse()

	list.insertAfterNode(9, 900)
	list.traverse()

	list.insertAfterNode(700, 7)
	list.traverse()

	list.deleteEndNode()
	list.traverse()

	list.deleteFrontNode()
	list.traverse()

	list.deleteNodeByPosition(0)
	list.traverse()

	list.deleteNodeByPosition(17)
	list.traverse()

	list.deleteNodeByPosition(3)
	list.traverse()

	list.deleteBeforeNode(200)
	list.traverse()

	list.deleteAfterNode(4)
	list.traverse()

	list.deleteAfterNode(100)
	list.traverse()

}
